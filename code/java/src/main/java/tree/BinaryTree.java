package tree;

import java.util.LinkedList;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BinaryTree {
	private static final Logger logger = LogManager.getLogger(LinkedList.class);


	public Node root = null;

	void printLevelOrder() {
		Queue<Node> queue = new LinkedList<Node>();
		queue.add(root);
		while (!queue.isEmpty()) {

			Node tempNode = queue.poll();
			System.out.print(tempNode.data + " ");

			/* Enqueue left child */
			if (tempNode.left != null) {
				queue.add(tempNode.left);
			}

			/* Enqueue right child */
			if (tempNode.right != null) {
				queue.add(tempNode.right);
			}
		}
	}

	void printPostorder(Node node) {
		if (node == null)
			return;

		// first recur on left subtree
		printPostorder(node.left);

		// then recur on right subtree
		printPostorder(node.right);

		// now deal with the node
		System.out.println(node.data + " ");
	}

	void printPerorder(Node node) {
		if (node == null)
			return;

		// now deal with the node
		System.out.println(node.data + " ");

		// first recur on left subtree
		printPostorder(node.left);

		// then recur on right subtree
		printPostorder(node.right);

	}

	void printInorder(Node node) {
		if (node == null)
			return;

		// first recur on left subtree
		printPostorder(node.left);

		// now deal with the node
		System.out.println(node.data + " ");

		// then recur on right subtree
		printPostorder(node.right);

	}

	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();

//				10
//			5		3
//		2	   9  1     6

		Node n10 = new Node(10, null, null);
		Node n5 = new Node(5, null, null);
		Node n3 = new Node(3, null, null);
		Node n2 = new Node(2, null, null);
		Node n9 = new Node(9, null, null);
		Node n1 = new Node(1, null, null);
		Node n6 = new Node(6, null, null);

		n5.setLeft(n2);
		n5.setRight(n9);
		n3.setLeft(n1);
		n3.setRight(n6);
		n10.setLeft(n5);
		n10.setRight(n3);

		tree.root = n10;

		logger.info("Printing post order");
		tree.printPostorder(tree.root);
		logger.info("Printting level order");
		
		tree.printLevelOrder();
	}

}

class Node {
	int data;
	Node left;
	Node right;
	Node parent;

	public Node(int data, Node left, Node right) {
		super();
		this.data = data;
		this.left = left;
		this.right = right;
	}


	@Override
	public String toString() {
		return "Node [data=" + data + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + data;
		result = prime * result + ((left == null) ? 0 : left.hashCode());
		result = prime * result + ((right == null) ? 0 : right.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (data != other.data)
			return false;
		if (left == null) {
			if (other.left != null)
				return false;
		} else if (!left.equals(other.left))
			return false;
		if (right == null) {
			if (other.right != null)
				return false;
		} else if (!right.equals(other.right))
			return false;
		return true;
	}

	public int getData() {
		return data;
	}

	public Node getLeft() {
		return left;
	}

	public Node getRight() {
		return right;
	}

	public void setData(int data) {
		this.data = data;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public void setRight(Node right) {
		this.right = right;
	}

}